package com.lucaslabari.ualatest.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.lucaslabari.ualatest.model.Book
import com.lucaslabari.ualatest.repositories.BooksRepository


class BooksViewModel(private val booksRepository: BooksRepository) : ViewModel()  {

    private var booksLiveData = booksRepository.getBooks()

    fun getBooksList(): LiveData<List<Book>> {
        return booksLiveData
    }


}