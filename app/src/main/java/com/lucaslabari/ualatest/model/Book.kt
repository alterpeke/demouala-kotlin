package com.lucaslabari.ualatest.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Book(
    val id: Int,
    val name: String,
    val author: String,
    val availability: Boolean,
    val popularity: Int,
    val image: String
) : Parcelable