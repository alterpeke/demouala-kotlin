package com.lucaslabari.ualatest.api.network_responses

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class BookResponse(
    @SerializedName("id")
    @Expose
    var id: Int,

    @SerializedName("nombre")
    @Expose
    var name: String,

    @SerializedName("autor")
    @Expose
    var author: String,

    @SerializedName("disponibilidad")
    @Expose
    var availability: Boolean,

    @SerializedName("popularidad")
    @Expose
    var popularity: Int,

    @SerializedName("imagen")
    @Expose
    var image: String
    ) {
        override fun toString(): String {
            return "BookResponse(id=$id, name='$name', author='$author', availability='$availability', popularity='$popularity',  image='$image')"
        }
    }