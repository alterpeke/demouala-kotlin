package com.lucaslabari.ualatest.api.network_responses

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class BooksListResponse (val results: List<BookResponse>)