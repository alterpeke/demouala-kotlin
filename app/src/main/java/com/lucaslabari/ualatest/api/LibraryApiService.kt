package com.lucaslabari.ualatest.api

import com.lucaslabari.ualatest.api.network_responses.BookResponse
import com.lucaslabari.ualatest.api.network_responses.BooksListResponse
import retrofit2.Call
import retrofit2.http.GET

interface LibraryApiService {

    @GET("/test/books")
    fun getBooks(): Call<List<BookResponse>>

}