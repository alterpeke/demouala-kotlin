package com.lucaslabari.ualatest.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.lucaslabari.ualatest.api.LibraryApiService
import com.lucaslabari.ualatest.api.network_responses.BookResponse
import com.lucaslabari.ualatest.api.network_responses.BooksListResponse
import com.lucaslabari.ualatest.model.Book
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BooksRepositoryImpl(private val libraryApiService: LibraryApiService) :  BooksRepository {

    override fun getBooks(): LiveData<List<Book>> {

        val booksList = MutableLiveData<List<Book>>()

        val call = libraryApiService.getBooks()

        call.enqueue(object : Callback<List<BookResponse>> {

            override fun onResponse(call: Call<List<BookResponse>>, response: Response<List<BookResponse>>) {
                if (response.isSuccessful()) {

                    val dataResponse = response.body()!!
                    val books = dataResponse.map {
                        Book(it.id, it.name, it.author, it.availability,
                            it.popularity, it.image)
                    }
                    booksList.setValue(books)
                }
            }

            override fun onFailure(call: Call<List<BookResponse>>, t: Throwable) {
                booksList.setValue(null)
            }

        })

        return booksList
    }

}