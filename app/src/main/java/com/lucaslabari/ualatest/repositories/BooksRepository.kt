package com.lucaslabari.ualatest.repositories

import androidx.lifecycle.LiveData
import com.lucaslabari.ualatest.model.Book

interface BooksRepository {

    fun getBooks(): LiveData<List<Book>>
}