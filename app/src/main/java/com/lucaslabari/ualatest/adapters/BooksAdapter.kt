package com.lucaslabari.ualatest.adapters


import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.lucaslabari.ualatest.ItemDetailActivity
import com.lucaslabari.ualatest.ItemDetailFragment
import com.lucaslabari.ualatest.ItemListActivity
import com.lucaslabari.ualatest.R
import com.lucaslabari.ualatest.model.Book
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_list_content.view.*
import java.util.concurrent.TimeUnit


class BooksAdapter(private val parentActivity: ItemListActivity,
                   private val twoPane: Boolean) :
    RecyclerView.Adapter<BooksAdapter.ViewHolder>(){

    private var values: List<Book> = ArrayList()
    private val onClickListener: View.OnClickListener

    init {
        onClickListener = View.OnClickListener { v ->
            val item = v.tag as Book
            if (twoPane) {
                val fragment = ItemDetailFragment().apply {
                    arguments = Bundle().apply {
                        putString(ItemDetailFragment.ARG_ITEM_ID, item.id.toString())
                    }
                }
                parentActivity.supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.item_detail_container, fragment)
                    .commit()
            } else {

                val intent = Intent(v.context, ItemDetailActivity::class.java).apply {
                    putExtra(ItemDetailFragment.ARG_ITEM_ID, item)
                }
                v.context.startActivity(intent)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list_content, parent, false)
        return ViewHolder(view)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.nameView.text = item.name
        holder.authorView.text = item.author

        val res = parentActivity.resources
        holder.availabilityView.text = if (item.availability) res.getString(R.string.book_available) else res.getString(R.string.book_not_available)
        holder.popularityView.text = item.popularity.toString()

        if (!item.image.isEmpty()){
            Picasso.get().load(item.image).into(holder.imgBook)
        }


        with(holder.itemView) {
            tag = item
            setOnClickListener(onClickListener)
        }
    }

    override fun getItemCount() = values.size

    fun setBooks(books: List<Book>) {
        values = books
        notifyDataSetChanged()
    }


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val nameView: TextView = view.txtName
        val authorView: TextView = view.txtAuthor
        val availabilityView: TextView = view.txtAvailability
        val popularityView: TextView = view.txtPopularity
        val imgBook: ImageView = view.imgThumbnail
    }






}
