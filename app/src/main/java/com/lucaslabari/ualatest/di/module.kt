package com.lucaslabari.ualatest.di

import com.lucaslabari.ualatest.api.LibraryApiService
import com.lucaslabari.ualatest.repositories.BooksRepository
import com.lucaslabari.ualatest.repositories.BooksRepositoryImpl
import com.lucaslabari.ualatest.util.Constants
import com.lucaslabari.ualatest.viewmodels.BooksViewModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val appModule = module {

    // Retrofit
    single { provideDefaultOkhttpClient() }
    single { provideRetrofit(get()) }
    single { provideLibraryApiService(get()) }

    // single instance of BooksRepository
    single<BooksRepository> {
        BooksRepositoryImpl(
            get()
        )
    }

    // OrderViewModel ViewModel
    viewModel { BooksViewModel(get()) }
}
fun provideDefaultOkhttpClient(): OkHttpClient {
    val interceptor = HttpLoggingInterceptor()
    interceptor.level = HttpLoggingInterceptor.Level.BODY
    return OkHttpClient.Builder()
        .addInterceptor(interceptor)
        .build()
}

fun provideRetrofit(client: OkHttpClient): Retrofit {
    return Retrofit.Builder()
        .baseUrl(Constants.LIBRARY_URL)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}

fun provideLibraryApiService(retrofit: Retrofit): LibraryApiService = retrofit.create(LibraryApiService::class.java)
