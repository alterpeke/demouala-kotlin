package com.lucaslabari.ualatest

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import android.widget.Toast
import androidx.lifecycle.Observer
import com.lucaslabari.ualatest.adapters.BooksAdapter

import com.lucaslabari.ualatest.model.Book
import com.lucaslabari.ualatest.viewmodels.BooksViewModel
import kotlinx.android.synthetic.main.activity_item_list.*
import kotlinx.android.synthetic.main.item_list.*
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * An activity representing a list of Pings. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a [ItemDetailActivity] representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
class ItemListActivity : AppCompatActivity() {

    private val booksViewModel: BooksViewModel by viewModel()
    private lateinit var booksAdapter: BooksAdapter

    private var twoPane: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_list)

        setSupportActionBar(toolbar)
        toolbar.title = title

        if (item_detail_container != null) {
            twoPane = true
        }

        //setupRecyclerView
        booksAdapter = BooksAdapter(this, twoPane)
        item_list.adapter = booksAdapter

        updateBooksList()

        showText(getString (R.string.welcome_message))

    }

    private fun updateBooksList(){

        // LiveData
        booksViewModel.getBooksList().observe(this, Observer<List<Book>> {
            booksAdapter.setBooks(it)
        })
    }


    private fun showText(text: String){
        Toast.makeText(this, text, Toast.LENGTH_LONG).show()
    }


    /**
     * Options Menu Creation
     */
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val menuInflate = menuInflater
        menuInflate.inflate(R.menu.menu_books_list, menu)
        return true
    }

    /**
     * Options Menu Item Selected
     */
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
//            R.id.remove_all -> {
//                removeAllPosts()
//            }
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return true
    }





}
